
#ifndef _SC_GPIB_
#define _SC_GPIB_

#define N_GPIB 1
#define GPIB_SETTINGS_STR(_name) char const *_name[] = {\
    "[DD]",\
    "MSCB Device = STRING : [32] mscb502",\
    "MSCB Pwd = STRING : [32] ",\
    "Base Address = INT : 1",\
    "",\
    "[.]",\
    "Names = STRING[9] :",\
    "[32] Rack1 OutTemp",\
    "[32] Rack1 InTemp",\
    "[32] Rack2 OutTemp",\
    "[32] Rack2 InTemp",\
    "[32] Rack3 OutTemp",\
    "[32] Rack3 inTemp",\
    "[32] Rack1 dTemp",\
    "[32] Rack2 dTemp",\
    "[32] Rack3 dTemp",\
    "",\
    NULL }

typedef struct {
  struct {
    char      mscb_device[32];
    char      mscb_pwd[32];
    INT       base_address;
  } dd;
  char      names[9][32];
} GPIB_SETTINGS;

GPIB_SETTINGS gpib_settings;

#endif
