/********************************************************************\
  Name:         scgpib.cxx
  Created by:   PAA
  Edited  by:   Lennart Huth <huth@physi.uni-heidelberg.de>
  $Id$
                GPIB through MSCB
\********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include "midas.h"
#include "mscb.h"
#include "OdbGpib.h"
#include <stdint.h>

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  /*-- Globals -------------------------------------------------------*/

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char const *frontend_name = "scGPIB";
  /* The frontend file name, don't change it */
  char const *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = TRUE;

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 0000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 10000;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */  INT max_event_size_frag = 5 * 1024 * 1024;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 10000;
  char const mydevname[] = {"GPIB410"};
  HNDLE hDB, hDD, hSet, hVar;
  int lmscb_fd;
  
  /* savefile */
  std::ofstream oFile;
  
  /*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
 
  INT read_trigger_event(char *pevent, INT off);
  INT read_scaler_event(char *pevent, INT off);
  INT read_mscb_event(char *pevent, INT off);
  INT localmscb_init(char const *eqname);

  INT set_voltage(float value, bool tout);
  INT read_voltage(bool tout);
  INT read_current(bool tout, char * current);

 /*-- Equipment list ------------------------------------------------*/

  EQUIPMENT equipment[] = {
    {"current",               /* equipment name */
     {15, 0,                   /* event ID, trigger mask */
      "SYSTEM",                /* event buffer */
      EQ_PERIODIC,             /* equipment type */
      0,                       /* event source */
      "MIDAS",                 /* format */
      TRUE,                    /* enabled */
      RO_ALWAYS |   /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      10000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", "",},
     read_mscb_event,       /* readout routine */
    },

    {""}
  };

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int status, size;
  char str[64];

  set_equipment_status("scgpib", "Initializing...", "#00FF00");  
  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */
  status = localmscb_init("scgpib");
  if (status != FE_SUCCESS) {
    cm_msg(MERROR,"scgpib","Access to mscb failed [%d]", status);
    return status;
  }
  
  // Get the hardware information
  sprintf (str, "%s", "*IDN?");
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 6);
  ss_sleep(1000);
  size = sizeof(str);
  status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);
  printf("Device: %s\n", str);

  //
  ss_sleep(1000);
  //sprintf (str, "%s", "CONF:CURR");
  //status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 9);
  sprintf(str,"%s","CONF:CURR");
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 10);
  
  //printf("localmscb_init status:%d\n %s\n", status,str);
  set_equipment_status("scgpib", "Ok", "#00FF00");  
  
  
  // open datafile to print in:
   oFile.open("test.txt");
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  set_equipment_status("scgpib", "Stopped...", "#00FF00");  
  printf("Stopped Frontend!\n");
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  /* put here clear scalers etc. */
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events
/*-- Trigger event routines ----------------------------------------*/

extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int i;
  DWORD lam;
  
  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
	return lam;
  }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  oFile.close();
  return SUCCESS;
}

/*-- MSCB event --------------------------------------------------*/
//int pt=0;
INT read_mscb_event(char *pevent, INT off)
{
  int   status, size;
  float fvalue[N_GPIB], *pfdata;
  char  str[64];
	//	static DWORD ptime = 0;

  //sprintf (str, "%s", "meas?");
  
  //status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 6);
  
  //ss_sleep(500);


  /* init bank structure */
  bk_init(pevent);
  
  /* create Gpib */
  //  bk_create(pevent, "CURR", TID_FLOAT, &pfdata);
  size = sizeof(str);
  //status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);
  //printf("Readback: %s\n", str);
  char current[14];
  for(float voltage = 25.0; voltage < 25.04; voltage+= 0.05)
  {
    status = set_voltage(voltage, true);
    if(status != voltage)
      printf("WARNING, WRONG VOLTAGE SET \n");
    
    for(int stat = 0; stat < 1; stat++)
    {
      
      status = read_voltage(true);
      status = read_current(true, &(current[0]));
      
      oFile << voltage << "\t" << current << "\n";
    }
  }
  
 
  
  
  //sprintf(str,"%s","SOUR:VOLT?");
  //status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 6);
  //ss_sleep(1000);
  //size = sizeof(str);
  //status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);
  //printf("returned voltage: %s \n",str);
  //ss_sleep(500);


  if (status != MSCB_SUCCESS) {
    cm_msg(MINFO,"scgpib","mscb_read failed [%d] on %d-ch%d"
	   , status, gpib_settings.dd.base_address, 6);
    set_equipment_status("scgpib", "mscb_read error", "#daa520");
   }
  
 

#if 0
  // Compute deltaT
  fvalue[6] = fvalue[0] - fvalue[1];
  fvalue[7] = fvalue[2] - fvalue[3];
  fvalue[8] = fvalue[4] - fvalue[5];
  // Flow 4l/min -> 0.066Kg/s
  // Cp : 4.18 KJ/Kg/C
  float dt2pw = 1000*4/60*4.18;
  
  //	printf("ss_time()-ptime:%d pt:%d pt%2:%d\n", ss_time()-ptime, pt, (pt%2));
  switch (ss_time() % 3) {
  case 0:
    sprintf(str, "H2O Pw[W]:%5.0f/%5.0f/%5.0f"
	    , dt2pw*fvalue[6], dt2pw*fvalue[7], dt2pw*fvalue[8]);
    break;
  case 1:
    sprintf(str, "H2O dT[C]:%5.1f/%5.1f/%5.1f", fvalue[6], fvalue[7], fvalue[8]);
   break;
  case 2:
    sprintf(str, "H2O Tin[C]:%5.1f/%5.1f/%5.1f", fvalue[0], fvalue[2], fvalue[4]);
    break;
  default:
    sprintf(str, "default\n");
    break;
  };
  
#endif
  
  // printf("%s\n", str);
  set_equipment_status("scgpib", str, "#00FF00");
  
  //	bk_close(pevent, pfdata);
  return bk_size(pevent);
}

/*-- Local MSCB event --------------------------------------------------*/
INT localmscb_init(char const *eqname)
{
  int  status, size;
  MSCB_INFO node_info;
  char set_str[80];
  GPIB_SETTINGS_STR(gpib_settings_str);
  
  cm_get_experiment_database(&hDB, NULL);
  
  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/%s/Settings", eqname);
  
  /* create PARAM settings record */
  status = db_create_record(hDB, 0, set_str, strcomb(gpib_settings_str));
  if (status != DB_SUCCESS)  return FE_ERR_ODB;
  
  status = db_find_key(hDB, 0, set_str, &hSet);
  status = db_find_key(hDB, hSet, "DD", &hDD);
  if (status == DB_SUCCESS) {
    size = sizeof(gpib_settings.dd);
    db_get_record(hDB, hDD, &(gpib_settings.dd), &size, 0);
    
    /* Open device on MSCB */
    lmscb_fd = mscb_init(gpib_settings.dd.mscb_device, sizeof(gpib_settings.dd.mscb_device)
			 , gpib_settings.dd.mscb_pwd, TRUE);
    printf("lmscb_fd: %d \n",lmscb_fd);
    if (lmscb_fd < 0) {
      cm_msg(MERROR, "mscb_init",
	     "Cannot access MSCB submaster at \"%s\". Check power and connection.",
	     gpib_settings.dd.mscb_device);
      return FE_ERR_HW;
    }
    
    // check if devices is alive 
#if 0
    status = mscb_ping(lmscb_fd, (unsigned short) gpib_settings.dd.base_address, 1);
    if (status != FE_SUCCESS) {
      cm_msg(MERROR, "mscb_init",
	     "Cannot ping MSCB address %d. Check power and connection.", gpib_settings.dd.base_address);
      //      return FE_ERR_HW;
    }
#endif
    
    // Check for right device
    status = mscb_info(lmscb_fd, (unsigned short) gpib_settings.dd.base_address, &node_info);
    if (strcmp(node_info.node_name, mydevname) != 0) {
      cm_msg(MERROR, "mscb_init",
	     "Found one expected node \"%s\" at address \"%d\"."
	     , node_info.node_name, gpib_settings.dd.base_address);
      return FE_ERR_HW;
    }
    printf("device %s found\n", node_info.node_name);
  }
  return FE_SUCCESS;
}


/*-- Set Voltage -------------------------------------------------------*/
INT set_voltage(float value, bool tout)
{
  int status;
  
  char  str[64];
  
  ss_sleep(1000);
  sprintf(str,"%s %g","SOUR:VOLT", value);
  if(tout)
    printf("Setting Volatge: %g\n",value);
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 14);
  ss_sleep(1000);
  
  return status;
}

/*-- Read Voltage ------------------------------------------------------*/
INT read_voltage(bool tout)
{
  int size, status;
  char  str[64];
  //sprintf (str, "%s", "CONF:VOLT");
  //status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 9);

  ss_sleep(1000);
  sprintf(str,"%s","SOUR:VOLT?");

  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 13);
  ss_sleep(1000);;

  size = sizeof(str);
  status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);

  ss_sleep(500); 
  if(tout)
    printf("Applied voltage: %s \n",str);
  return status;

}

/*-- Read Current ------------------------------------------------------*/

INT read_current(bool tout, char * current)
{
  int status, size;
  char str[64];

  //sprintf (str, "%s", "CONF:CURR");                                                                    
  //status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 9); 

  ss_sleep(1000);

  sprintf(str,"%s","MEAS?");
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 6);
  ss_sleep(1000);
  size = sizeof(str);
  status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);

  if(tout)
    printf("Applied Current: %s \n",str);
  current = &(str[0]);
  ss_sleep(500);


  return status;
}
