#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     UNIX Makefile for MIDAS slow control frontend
#
#  $Id: Makefile 2779 2005-10-19 13:14:36Z ritt $
#
#####################################################################

#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the 
# lines below.

# This is for Linux ----------------
LIBS = -lbsd -lm -lutil -lpthread -lrt
OSFLAGS = -DOS_LINUX -DHAVE_LIBUSB -DHAVE_USB

# This is for OSF1 -----------------
#LIBS = -lbsd
#OSFLAGS = -DOS_OSF1

# This is for Ultrix ---------------
#LIBS =
#OSFLAGS = -DOS_ULTRIX -Dextname

# This is for FreeBSD --------------
#LIBS = -lbsd -lcompat
#OSFLAGS = -DOS_FREEBSD

#-------------------------------------------------------------------
# The following lines define direcories. Adjust if necessary
#                 
INC_DIR 	= $(MIDASSYS)/include
LIB_DIR 	= $(MIDASSYS)/linux/lib
DRV_DIR		= $(MIDASSYS)/drivers
MSCB_DIR        = $(MIDASSYS)/../mscb

#-------------------------------------------------------------------
# Drivers needed by the frontend program
#                 
DRIVERS         =  strlcpy.o mxml.o mscb.o musbstd.o 

####################################################################
# Lines below here should not be edited
####################################################################

LIB = $(LIB_DIR)/libmidas.a -lusb

# compiler
CC = cc
CXX = c++
CFLAGS = -g -Wall -DUNIX -I. -I$(MIDASSYS)/../mxml -I$(INC_DIR) -I$(DRV_DIR) -I$(MSCB_DIR)
LDFLAGS =

all: scgpib.exe

scgpib.exe:  $(LIB) $(LIB_DIR)/mfe.o scgpib.o $(DRIVERS)
	$(CXX) -o $@ scgpib.o $(LIB_DIR)/mfe.o $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

mscbdev.o: $(DRV_DIR)/device/mscbdev.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
musbstd.o: $(DRV_DIR)/usb/musbstd.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mscbrpc.o: $(MSCB_DIR)/mscbrpc.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mscb.o: $(MSCB_DIR)/mscb.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mxml.o: $(MIDASSYS)/../mxml/mxml.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
strlcpy.o: $(MIDASSYS)/../mxml/strlcpy.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

scgpib.o: VoltageScan.cxx
	$(CXX) $(CFLAGS) $(OSFLAGS) -c $< -o $@

%.o: %.cxx experim.h
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

.c.o:
	$(CC) $(CFLAGS)  $(OSFLAGS) -c $<

$(LIB) $(LIB_DIR)/mfe.o:
	@cd $(MIDASSYS) && make

clean:
	rm -f *.o *.exe *~ \#*

